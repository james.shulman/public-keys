# public-keys

**Repository:** https://gitlab.com/james.shulman/public-keys

**Description:** This repository has been made public and contains my **public** GPG & SSH keys used for encryption and secure communication

**Key Information:**
- <a href="dollarteaclub_jamesshulman.gpg.asc">dollarteaclub_jamesshulman.gpg.asc</a> --> Primary GPG key
  - SHA1: <a href="dollarteaclub_jamesshulman.gpg.asc.sha1">dollarteaclub_jamesshulman.gpg.asc.sha1</a>
  - Type: rsa4096

<br>
<hr>
Dollar Tea Club Website: <a href="https://www.dollarteaclub.com">www.dollarteaclub.com</a>
<br>
For any change inquiries/suggestions, please email <a href="mailto:james.shulman@dollarteaclub.com">james.shulman@dollarteaclub.com</a>